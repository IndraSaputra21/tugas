<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{ asset('/admin/dist/img/maxindo.jpg')}} "
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Maxindo Maxmon Nih</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        <img src="{{ asset('/admin/dist/img/indra.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Indra Saputra</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="/homee" class="nav-link">
            <i class="nav-icon fas fa-home"></i>
            <p>Home</p></a>
          </li>

          <li class="nav-item">
            <a href="/data-table" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>Data Tables</p></a>
          </li>

          
          
        </ul>
      </nav>
      
    </div>
    
  </aside>