<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'homeController@index');
Route::get('/Register', 'authController@register');
Route::get('/welcom', 'authController@welcom');

Route::get('/master', function () {
	return view('template.master');

});

Route::get('data-table', function() {
		return view('template.data-table	');

		});
 /*rute template */
Route::get('homee', function() {
		return view('template.homee	');

		});
